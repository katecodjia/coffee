# coffee app with Flutter (iOS and Android)


## How to Start

<ul>
  <li>Check that https://gitlab.com/katecodjia/coffee.git</li>
    <li>flutter clean</li>
    <li>flutter packages get</li>
    <li>flutter run</li>
</ul>

# Tested on
- iOS Simulator 14 Plus
- Android Simulator Pixel 5


# Screenshots & Gifs


<img src="assets/images/home.png" alt="home" width="350"/>
<img src="assets/images/coffee-list.png" alt="list of coffee" width="350"/>
<img src="assets/images/details.png" alt="Details" width="350"/>
![gif](/uploads/c1ff1d71b33f8fffcc6e0ca5bad3a4e9/gif.mov)



