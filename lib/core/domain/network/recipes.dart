class Recipes {
  String? q;
  int? from;
  int? to;
  bool? more;
  int? count;
  List<Hits>? hits;

  Recipes({required this.q, required this.from, required this.to, required this.more, required this.count, required this.hits});

  Recipes.fromJson(Map<String, dynamic> json) {
    q = json['q'];
    from = json['from'];
    to = json['to'];
    more = json['more'];
    count = json['count'];
    if (json['hits'] != null) {
      hits = <Hits>[];
      json['hits'].forEach((v) {
        hits?.add(Hits.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['q'] = q;
    data['from'] = from;
    data['to'] = to;
    data['more'] = more;
    data['count'] = count;
    final hits = this.hits;
    if (hits != null) {
      data['hits'] = hits.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Hits {
  Recipe? recipe;

  Hits({required this.recipe});

  Hits.fromJson(Map<String, dynamic> json) {
    recipe =
    (json['recipe'] != null ?  Recipe.fromJson(json['recipe']) : null);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data =  <String, dynamic>{};
    final recipe = this.recipe;
    if (recipe != null) {
      data['recipe'] = recipe.toJson();
    }
    return data;
  }
}

class Recipe {
  String? uri;
  String? label;
  String? image;
  String? source;
  String? url;
  String? shareAs;
  double? yield;
  List<String>? dietLabels;
  List<String>? healthLabels;
  List<String>? cautions;
  List<String>? ingredientLines;
  List<Ingredients>? ingredients;
  double? calories;
  double? totalWeight;
  double? totalTime;
  List<String>? cuisineType;
  List<String>? mealType;
  List<String>? dishType;
  TotalNutrients? totalNutrients;
  TotalDaily? totalDaily;
  List<Digest>? digest;

  Recipe(
      {required this.uri,
        required this.label,
        required this.image,
        required  this.source,
        required  this.url,
        required  this.shareAs,
        required  this.yield,
        required  this.dietLabels,
          required  this.healthLabels,
        required  this.cautions,
        required this.ingredientLines,
        required  this.ingredients,
        required this.calories,
        required  this.totalWeight,
        required this.totalTime,
        required  this.cuisineType,
        required  this.mealType,
        required   this.dishType,
        required   this.totalNutrients,
        required   this.totalDaily,
        required   this.digest});

  Recipe.fromJson(Map<String, dynamic> json) {
    uri = json['uri'];
    label = json['label'];
    image = json['image'];
    source = json['source'];
    url = json['url'];
    shareAs = json['shareAs'];
    yield = json['yield'];
    dietLabels = json['dietLabels'].cast<String>();
    healthLabels = json['healthLabels'].cast<String>();
    cautions = json['cautions'].cast<String>();
    ingredientLines = json['ingredientLines'].cast<String>();
    if (json['ingredients'] != null) {
      ingredients =  <Ingredients>[];
      json['ingredients'].forEach((v) {
        ingredients?.add(Ingredients.fromJson(v));
      });
    }
    calories = json['calories'];
    totalWeight = json['totalWeight'];
    totalTime = json['totalTime'];
    cuisineType = json['cuisineType'].cast<String>();
    mealType = json['mealType'].cast<String>();
    dishType = json['dishType'] != null
        ?json['dishType'].cast<String>()
        :null;
    totalNutrients = (json['totalNutrients'] != null
        ? TotalNutrients.fromJson(json['totalNutrients'])
        : null)!;
    totalDaily = (json['totalDaily'] != null
        ? TotalDaily.fromJson(json['totalDaily'])
        : null)!;
    if (json['digest'] != null) {
      digest = <Digest>[];
      json['digest'].forEach((v) {
        digest?.add(Digest.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['uri'] = uri;
    data['label'] = label;
    data['image'] = image;
    data['source'] = source;
    data['url'] = url;
    data['shareAs'] = shareAs;
    data['yield'] = yield;
    data['dietLabels'] = dietLabels;
    data['healthLabels'] = healthLabels;
    data['cautions'] = cautions;
    data['ingredientLines'] = ingredientLines;
    data['ingredients'] = ingredients?.map((v) => v.toJson()).toList();
    data['calories'] =calories;
    data['totalWeight'] = totalWeight;
    data['totalTime'] = totalTime;
    data['cuisineType'] =cuisineType;
    data['mealType'] = mealType;
    data['dishType'] = dishType;
    final totalNutrients = this.totalNutrients;
    if (totalNutrients != null) {
      data['totalNutrients'] = totalNutrients.toJson();
    }
    if (this.totalDaily != null) {
      data['totalDaily'] = totalDaily?.toJson();
    }
    final digest = this.digest;
    if (digest != null) {
      data['digest'] = digest.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Ingredients {
  String? text;
  double? quantity;
  String? measure;
  String? food;
  double? weight;
  String? foodCategory;
  String? foodId;
  String? image;

  Ingredients(
      {required this.text,
        required this.quantity,
        required this.measure,
        required this.food,
        required this.weight,
        required this.foodCategory,
        required this.foodId,
        required this.image});

  Ingredients.fromJson(Map<String, dynamic> json) {
    text = json['text'];
    quantity = json['quantity'];
    measure = json['measure'];
    food = json['food'];
    weight = json['weight'];
    foodCategory = json['foodCategory'];
    foodId = json['foodId'];
    image = json['image'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['text'] = text;
    data['quantity'] = quantity;
    data['measure'] = measure;
    data['food'] = food;
    data['weight'] = weight;
    data['foodCategory'] = foodCategory;
    data['foodId'] = foodId;
    data['image'] = this.image;
    return data;
  }
}

class TotalNutrients {
  ENERCKCAL? eNERCKCAL;
  ENERCKCAL? fAT;
  ENERCKCAL? fASAT;
  ENERCKCAL? fATRN;
  ENERCKCAL? fAMS;
  ENERCKCAL? fAPU;
  ENERCKCAL? cHOCDF;
  ENERCKCAL? cHOCDFNet;
  ENERCKCAL? fIBTG;
  ENERCKCAL? sUGAR;
  SUGARAdded? sUGARAdded;
  ENERCKCAL? pROCNT;
  ENERCKCAL? cHOLE;
  ENERCKCAL? nA;
  ENERCKCAL? cA;
  ENERCKCAL? mG;
  ENERCKCAL? k;
  ENERCKCAL? fE;
  ENERCKCAL? zN;
  ENERCKCAL? p;
  ENERCKCAL? vITARAE;
  ENERCKCAL? vITC;
  ENERCKCAL? tHIA;
  ENERCKCAL? rIBF;
  ENERCKCAL? nIA;
  ENERCKCAL? vITB6A;
  ENERCKCAL? fOLDFE;
  ENERCKCAL? fOLFD;
  SUGARAdded? fOLAC;
  ENERCKCAL? vITB12;
  ENERCKCAL? vITD;
  ENERCKCAL? tOCPHA;
  ENERCKCAL? vITK1;
  SUGARAdded? sugarAlcohol;
  ENERCKCAL? wATER;

  TotalNutrients(
      {required this.eNERCKCAL,
        required  this.fAT,
        required this.fASAT,
        required this.fATRN,
        required this.fAMS,
        required this.fAPU,
        required this.cHOCDF,
        required  this.cHOCDFNet,
         required  this.fIBTG,
        required this.sUGAR,
        required this.sUGARAdded,
        required this.pROCNT,
        required  this.cHOLE,
        required this.nA,
        required  this.cA,
        required this.mG,
        required this.k,
        required this.fE,
        required this.zN,
        required this.p,
        required this.vITARAE,
        required  this.vITC,
        required this.tHIA,
        required this.rIBF,
        required this.nIA,
        required this.vITB6A,
        required this.fOLDFE,
         required this.fOLFD,
        required this.fOLAC,
        required  this.vITB12,
        required this.vITD,
        required this.tOCPHA,
        required this.vITK1,
        required  this.sugarAlcohol,
        required  this.wATER});

  TotalNutrients.fromJson(Map<String, dynamic> json) {
    eNERCKCAL = (json['ENERC_KCAL'] != null
        ? ENERCKCAL.fromJson(json['ENERC_KCAL'])
        : null)!;
    fAT = (json['FAT'] != null ? ENERCKCAL.fromJson(json['FAT']) : null);
    fASAT =
    (json['FASAT'] != null ?  ENERCKCAL.fromJson(json['FASAT']) : null);
    fATRN =
    (json['FATRN'] != null ?  ENERCKCAL.fromJson(json['FATRN']) : null);
    fAMS = (json['FAMS'] != null ?  ENERCKCAL.fromJson(json['FAMS']) : null);
    fAPU = (json['FAPU'] != null ?  ENERCKCAL.fromJson(json['FAPU']) : null);
    cHOCDF =
    (json['CHOCDF'] != null ? new ENERCKCAL.fromJson(json['CHOCDF']) : null);
    cHOCDFNet = (json['CHOCDF.net'] != null
        ? new ENERCKCAL.fromJson(json['CHOCDF.net'])
        : null);
    fIBTG =
    (json['FIBTG'] != null ? new ENERCKCAL.fromJson(json['FIBTG']) : null);
    sUGAR =
    (json['SUGAR'] != null ? new ENERCKCAL.fromJson(json['SUGAR']) : null);
    sUGARAdded = (json['SUGAR.added'] != null
        ? new SUGARAdded.fromJson(json['SUGAR.added'])
        : null);
    pROCNT =
    (json['PROCNT'] != null ? new ENERCKCAL.fromJson(json['PROCNT']) : null);
    cHOLE =
    (json['CHOLE'] != null ? new ENERCKCAL.fromJson(json['CHOLE']) : null);
    nA = (json['NA'] != null ? new ENERCKCAL.fromJson(json['NA']) : null);
    cA = (json['CA'] != null ? new ENERCKCAL.fromJson(json['CA']) : null);
    mG = (json['MG'] != null ? new ENERCKCAL.fromJson(json['MG']) : null);
    k = (json['K'] != null ? new ENERCKCAL.fromJson(json['K']) : null);
    fE = (json['FE'] != null ? new ENERCKCAL.fromJson(json['FE']) : null);
    zN = (json['ZN'] != null ? new ENERCKCAL.fromJson(json['ZN']) : null);
    p = (json['P'] != null ? new ENERCKCAL.fromJson(json['P']) : null);
    vITARAE = (json['VITA_RAE'] != null
        ? new ENERCKCAL.fromJson(json['VITA_RAE'])
        : null)!;
    vITC = (json['VITC'] != null ? new ENERCKCAL.fromJson(json['VITC']) : null)!;
    tHIA = (json['THIA'] != null ? new ENERCKCAL.fromJson(json['THIA']) : null)!;
    rIBF = (json['RIBF'] != null ? new ENERCKCAL.fromJson(json['RIBF']) : null)!;
    nIA = (json['NIA'] != null ? new ENERCKCAL.fromJson(json['NIA']) : null)!;
    vITB6A =
    (json['VITB6A'] != null ? new ENERCKCAL.fromJson(json['VITB6A']) : null)!;
    fOLDFE =
    (json['FOLDFE'] != null ? new ENERCKCAL.fromJson(json['FOLDFE']) : null)!;
    fOLFD =
    (json['FOLFD'] != null ? new ENERCKCAL.fromJson(json['FOLFD']) : null)!;
    fOLAC =
    (json['FOLAC'] != null ? new SUGARAdded.fromJson(json['FOLAC']) : null)!;
    vITB12 =
    (json['VITB12'] != null ? new ENERCKCAL.fromJson(json['VITB12']) : null)!;
    vITD = (json['VITD'] != null ? new ENERCKCAL.fromJson(json['VITD']) : null)!;
    tOCPHA =
    (json['TOCPHA'] != null ? new ENERCKCAL.fromJson(json['TOCPHA']) : null)!;
    vITK1 =
    (json['VITK1'] != null ? new ENERCKCAL.fromJson(json['VITK1']) : null)!;
    sugarAlcohol = (json['Sugar.alcohol'] != null
        ? new SUGARAdded.fromJson(json['Sugar.alcohol'])
        : null)!;
    wATER =
    (json['WATER'] != null ? new ENERCKCAL.fromJson(json['WATER']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    final eNERCKCAL = this.eNERCKCAL;
    if (eNERCKCAL != null) {
      data['ENERC_KCAL'] = eNERCKCAL.toJson();
    }
    final fAT = this.fAT;
    if (fAT != null) {
      data['FAT'] = fAT.toJson();
    }
    final fASAT = this.fASAT;
    if (fASAT != null) {
      data['FASAT'] = fASAT.toJson();
    }
    final fATRN = this.fATRN;
    if (fATRN != null) {
      data['FATRN'] = fATRN.toJson();
    }
    final fAMS = this.fAMS;
    if (fAMS != null) {
      data['FAMS'] = fAMS.toJson();
    }
    final fAPU = this.fAPU;
    if (fAPU != null) {
      data['FAPU'] = fAPU.toJson();
    }
    final cHOCDF = this.cHOCDF;
    if (cHOCDF != null) {
      data['CHOCDF'] = cHOCDF.toJson();
    }
    final cHOCDFNet = this.cHOCDFNet;
    if (cHOCDFNet != null) {
      data['CHOCDF.net'] = cHOCDFNet.toJson();
    }
    final fIBTG = this.fIBTG;
    if (fIBTG != null) {
      data['FIBTG'] = fIBTG.toJson();
    }
    final sUGAR = this.sUGAR;
    if (sUGAR != null) {
      data['SUGAR'] = sUGAR.toJson();
    }
    final sUGARAdded = this.sUGARAdded;
    if (sUGARAdded != null) {
      data['SUGAR.added'] = sUGARAdded.toJson();
    }
    final pROCNT = this.pROCNT;
    if (pROCNT != null) {
      data['PROCNT'] = pROCNT.toJson();
    }
    final cHOLE = this.cHOLE;
    if (cHOLE != null) {
      data['CHOLE'] = cHOLE.toJson();
    }
    final nA = this.nA;
    if (nA != null) {
      data['NA'] = nA.toJson();
    }
    final cA = this.cA;
    if (cA != null) {
      data['CA'] = cA.toJson();
    }
    final mG = this.mG;
    if (mG != null) {
      data['MG'] = mG.toJson();
    }
    final k = this.k;
    if (k != null) {
      data['K'] = k.toJson();
    }
    final fE = this.fE;
    if (fE != null) {
      data['FE'] = fE.toJson();
    }
    final zN = this.zN;
    if (zN != null) {
      data['ZN'] = zN.toJson();
    }
    final p = this.p;
    if (p != null) {
      data['P'] = p.toJson();
    }
    final vITARAE = this.vITARAE;
    if (vITARAE != null) {
      data['VITA_RAE'] = vITARAE.toJson();
    }
    final vITC = this.vITC;
    if (vITC != null) {
      data['VITC'] = vITC.toJson();
    }
    final tHIA = this.tHIA;
    if (tHIA != null) {
      data['THIA'] = tHIA.toJson();
    }
    final rIBF = this.rIBF;
    if (rIBF != null) {
      data['RIBF'] = rIBF.toJson();
    }
    final nIA = this.nIA;
    if (nIA != null) {
      data['NIA'] = nIA.toJson();
    }
    final vITB6A = this.vITB6A;
    if (vITB6A != null) {
      data['VITB6A'] = vITB6A.toJson();
    }
    final fOLDFE = this.fOLDFE;
    if (fOLDFE != null) {
      data['FOLDFE'] = fOLDFE.toJson();
    }
    final fOLFD = this.fOLFD;
    if (fOLFD != null) {
      data['FOLFD'] = fOLFD.toJson();
    }
    final fOLAC = this.fOLAC;
    if (fOLAC != null) {
      data['FOLAC'] = fOLAC.toJson();
    }
    final vITB12 = this.vITB12;
    if (vITB12 != null) {
      data['VITB12'] = vITB12.toJson();
    }
    final vITD = this.vITD;
    if (vITD != null) {
      data['VITD'] = vITD.toJson();
    }
    final tOCPHA = this.tOCPHA;
    if (tOCPHA != null) {
      data['TOCPHA'] = tOCPHA.toJson();
    }
    final vITK1 = this.vITK1;
    if (vITK1 != null) {
      data['VITK1'] = vITK1.toJson();
    }
    final sugarAlcohol = this.sugarAlcohol;
    if (sugarAlcohol != null) {
      data['Sugar.alcohol'] = sugarAlcohol.toJson();
    }
    final wATER = this.wATER;
    if (wATER != null) {
      data['WATER'] = wATER.toJson();
    }
    return data;
  }
}

class ENERCKCAL {
  String? label;
  double? quantity;
  String? unit;

  ENERCKCAL({required this.label, required this.quantity, required this.unit});

  ENERCKCAL.fromJson(Map<String, dynamic> json) {
    label = json['label'];
    quantity = json['quantity'];
    unit = json['unit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['label'] = label;
    data['quantity'] = quantity;
    data['unit'] = unit;
    return data;
  }
}

class SUGARAdded {
  String? label;
  double? quantity;
  String? unit;

  SUGARAdded({required this.label, required this.quantity, required this.unit});

  SUGARAdded.fromJson(Map<String, dynamic> json) {
    label = json['label'];
    quantity = json['quantity'];
    unit = json['unit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['label'] = label;
    data['quantity'] = quantity;
    data['unit'] = unit;
    return data;
  }
}

class TotalDaily {
  ENERCKCAL? eNERCKCAL;
  ENERCKCAL? fAT;
  ENERCKCAL? fASAT;
  ENERCKCAL? cHOCDF;
  ENERCKCAL? fIBTG;
  ENERCKCAL? pROCNT;
  ENERCKCAL? cHOLE;
  ENERCKCAL? nA;
  ENERCKCAL? cA;
  ENERCKCAL? mG;
  ENERCKCAL? k;
  ENERCKCAL? fE;
  ENERCKCAL? zN;
  ENERCKCAL? p;
  ENERCKCAL? vITARAE;
  ENERCKCAL? vITC;
  ENERCKCAL? tHIA;
  ENERCKCAL? rIBF;
  ENERCKCAL? nIA;
  ENERCKCAL? vITB6A;
  ENERCKCAL? fOLDFE;
  ENERCKCAL? vITB12;
  ENERCKCAL? vITD;
  ENERCKCAL? tOCPHA;
  ENERCKCAL? vITK1;

  TotalDaily(
      {required this.eNERCKCAL,
        required this.fAT,
        required this.fASAT,
        required this.cHOCDF,
        required this.fIBTG,
        required this.pROCNT,
        required this.cHOLE,
        required this.nA,
        required this.cA,
        required this.mG,
        required this.k,
        required this.fE,
        required this.zN,
        required this.p,
        required this.vITARAE,
        required this.vITC,
        required this.tHIA,
        required this.rIBF,
        required this.nIA,
        required this.vITB6A,
        required this.fOLDFE,
        required this.vITB12,
        required this.vITD,
        required this.tOCPHA,
        required this.vITK1});

  TotalDaily.fromJson(Map<String, dynamic> json) {
    eNERCKCAL = (json['ENERC_KCAL'] != null
        ? new ENERCKCAL.fromJson(json['ENERC_KCAL'])
        : null)!;
    fAT = (json['FAT'] != null ? new ENERCKCAL.fromJson(json['FAT']) : null)!;
    fASAT =
    (json['FASAT'] != null ? new ENERCKCAL.fromJson(json['FASAT']) : null)!;
    cHOCDF =
    (json['CHOCDF'] != null ? new ENERCKCAL.fromJson(json['CHOCDF']) : null)!;
    fIBTG =
    (json['FIBTG'] != null ? new ENERCKCAL.fromJson(json['FIBTG']) : null)!;
    pROCNT =
    (json['PROCNT'] != null ? new ENERCKCAL.fromJson(json['PROCNT']) : null)!;
    cHOLE =
    (json['CHOLE'] != null ? new ENERCKCAL.fromJson(json['CHOLE']) : null)!;
    nA = (json['NA'] != null ? new ENERCKCAL.fromJson(json['NA']) : null)!;
    cA = (json['CA'] != null ? new ENERCKCAL.fromJson(json['CA']) : null)!;
    mG = (json['MG'] != null ? new ENERCKCAL.fromJson(json['MG']) : null)!;
    k = (json['K'] != null ? new ENERCKCAL.fromJson(json['K']) : null)!;
    fE = (json['FE'] != null ? new ENERCKCAL.fromJson(json['FE']) : null)!;
    zN = (json['ZN'] != null ? new ENERCKCAL.fromJson(json['ZN']) : null)!;
    p = (json['P'] != null ? new ENERCKCAL.fromJson(json['P']) : null)!;
    vITARAE = (json['VITA_RAE'] != null
        ? ENERCKCAL.fromJson(json['VITA_RAE'])
        : null)!;
    vITC = (json['VITC'] != null ? new ENERCKCAL.fromJson(json['VITC']) : null)!;
    tHIA = (json['THIA'] != null ? new ENERCKCAL.fromJson(json['THIA']) : null)!;
    rIBF = (json['RIBF'] != null ? new ENERCKCAL.fromJson(json['RIBF']) : null)!;
    nIA = (json['NIA'] != null ? new ENERCKCAL.fromJson(json['NIA']) : null)!;
    vITB6A =
    (json['VITB6A'] != null ? new ENERCKCAL.fromJson(json['VITB6A']) : null)!;
    fOLDFE =
    (json['FOLDFE'] != null ? new ENERCKCAL.fromJson(json['FOLDFE']) : null)!;
    vITB12 =
    (json['VITB12'] != null ? new ENERCKCAL.fromJson(json['VITB12']) : null)!;
    vITD = (json['VITD'] != null ? new ENERCKCAL.fromJson(json['VITD']) : null)!;
    tOCPHA =
    (json['TOCPHA'] != null ? new ENERCKCAL.fromJson(json['TOCPHA']) : null)!;
    vITK1 =
    (json['VITK1'] != null ? new ENERCKCAL.fromJson(json['VITK1']) : null)!;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    final eNERCKCAL = this.eNERCKCAL;
    if (eNERCKCAL != null) {
      data['ENERC_KCAL'] = eNERCKCAL.toJson();
    }
    final fAT = this.fAT;
    if (fAT != null) {
      data['FAT'] = fAT.toJson();
    }
    final fASAT = this.fASAT;
    if (fASAT != null) {
      data['FASAT'] = fASAT.toJson();
    }
    final cHOCDF = this.cHOCDF;
    if (cHOCDF != null) {
      data['CHOCDF'] = cHOCDF.toJson();
    }
    final fIBTG = this.fIBTG;
    if (fIBTG != null) {
      data['FIBTG'] = fIBTG.toJson();
    }
    final pROCNT = this.pROCNT;
    if (pROCNT != null) {
      data['PROCNT'] = pROCNT.toJson();
    }
    final cHOLE = this.cHOLE;
    if (cHOLE != null) {
      data['CHOLE'] = cHOLE.toJson();
    }
    final nA = this.nA;
    if (nA != null) {
      data['NA'] = nA.toJson();
    }
    final cA = this.cA;
    if (cA != null) {
      data['CA'] = cA.toJson();
    }
    final mG = this.mG;
    if (mG != null) {
      data['MG'] = mG.toJson();
    }
    final k = this.k;
    if (k != null) {
      data['K'] = k.toJson();
    }
    final fE = this.fE;
    if (fE != null) {
      data['FE'] = fE.toJson();
    }
    final zN = this.zN;
    if (zN != null) {
      data['ZN'] = zN.toJson();
    }
    final p = this.p;
    if (p != null) {
      data['P'] = p.toJson();
    }
    final vITARAE = this.vITARAE;
    if (vITARAE != null) {
      data['VITA_RAE'] = vITARAE.toJson();
    }
    final vITC = this.vITC;
    if (vITC != null) {
      data['VITC'] = vITC.toJson();
    }
    final tHIA = this.tHIA;
    if (tHIA != null) {
      data['THIA'] = tHIA.toJson();
    }
    final rIBF = this.rIBF;
    if (rIBF != null) {
      data['RIBF'] = rIBF.toJson();
    }
    final nIA = this.nIA;
    if (nIA != null) {
      data['NIA'] = nIA.toJson();
    }
    final vITB6A = this.vITB6A;
    if (vITB6A != null) {
      data['VITB6A'] = vITB6A.toJson();
    }
    final fOLDFE = this.fOLDFE;
    if (fOLDFE != null) {
      data['FOLDFE'] = fOLDFE.toJson();
    }
    final vITB12 = this.vITB12;
    if (vITB12 != null) {
      data['VITB12'] = vITB12.toJson();
    }
    final vITD = this.vITD;
    if (vITD != null) {
      data['VITD'] = vITD.toJson();
    }
    final tOCPHA = this.tOCPHA;
    if (tOCPHA != null) {
      data['TOCPHA'] = tOCPHA.toJson();
    }
    final vITK1 = this.vITK1;
    if (vITK1 != null) {
      data['VITK1'] = vITK1.toJson();
    }
    return data;
  }
}

class Digest {
  String? label;
  String? tag;
  String? schemaOrgTag;
  double? total;
  bool? hasRDI;
  double? daily;
  String? unit;
  List<Sub>? sub;

  Digest(
      {required this.label,
        required this.tag,
        required this.schemaOrgTag,
        required this.total,
        required this.hasRDI,
        required this.daily,
        required this.unit,
        required this.sub});

  Digest.fromJson(Map<String, dynamic> json) {
    label = json['label'];
    tag = json['tag'];
    schemaOrgTag = json['schemaOrgTag'];
    total = json['total'];
    hasRDI = json['hasRDI'];
    daily = json['daily'];
    unit = json['unit'];
    if (json['sub'] != null) {
      sub = <Sub>[];
      json['sub'].forEach((v) {
        sub?.add(Sub.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['label'] = label;
    data['tag'] = tag;
    data['schemaOrgTag'] = schemaOrgTag;
    data['total'] = total;
    data['hasRDI'] = hasRDI;
    data['daily'] = daily;
    data['unit'] = unit;
    if (sub != null) {
      data['sub'] = sub?.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class Sub {
  String? label;
  String? tag;
  String? schemaOrgTag;
  double? total;
  bool? hasRDI;
  double? daily;
  String? unit;

  Sub(
      {required this.label,
        required this.tag,
        required this.schemaOrgTag,
        required this.total,
        required this.hasRDI,
        required this.daily,
        required this.unit});

  Sub.fromJson(Map<String, dynamic> json) {
    label = json['label'];
    tag = json['tag'];
    schemaOrgTag = json['schemaOrgTag'];
    total = json['total'];
    hasRDI = json['hasRDI'];
    daily = json['daily'];
    unit = json['unit'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['label'] = label;
    data['tag'] = tag;
    data['schemaOrgTag'] = schemaOrgTag;
    data['total'] = total;
    data['hasRDI'] = hasRDI;
    data['daily'] = daily;
    data['unit'] = this.unit;
    return data;
  }
}
