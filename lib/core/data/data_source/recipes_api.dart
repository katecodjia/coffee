import 'dart:developer';
import 'package:dio/dio.dart';
import '../../domain/network/recipes.dart';

class SearchRecipesApi{
  final Dio _dio = Dio();

  Future<Recipes?> getSearch(page) async {
     String _url  = "https://api.edamam.com/search?q=chicken&app_id=232d458a&app_key=d7140a99e3fb5e29c6d811f46685ea0f&from=0&to=$page&calories=591-722&health=alcohol-free";

    try {
      Response response = await _dio.get(_url);
      if (response.statusCode == 200) {
        return Recipes.fromJson(response.data);
      } else {
        return null;
      }
    } on DioError catch (e) {
      log("error1===>$e");
      return null;
    }
  }
  Future<Recipes?> getSearchData(search) async {
    final String url  = "https://api.edamam.com/search?q=$search&app_id=232d458a&app_key=d7140a99e3fb5e29c6d811f46685ea0f&from=0&to=100&calories=591-722&health=alcohol-free";
    try {
      Response response = await _dio.get(url);
      if (response.statusCode == 200) {
        return Recipes.fromJson(response.data);
      } else {
        return null;
      }
    } on DioError catch (e) {
      log("error1===>$e");
      return null;
    }
  }

}