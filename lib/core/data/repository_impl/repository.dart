import '../../domain/network/recipes.dart';
import '../data_source/recipes_api.dart';

class ApiRepository{
  SearchRecipesApi searchRecipesApi  = SearchRecipesApi();
  Future<Recipes?> getRecipesSearchApi(page){
    return searchRecipesApi.getSearch(page);
  }
  Future<Recipes?> getSearchDataApi(search){
    return searchRecipesApi.getSearchData(search);
  }
}
