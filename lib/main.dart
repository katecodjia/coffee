import 'package:cofee/views/pages/main/main_coffee.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'app.dart';
import 'package:flutter_phoenix/flutter_phoenix.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

Future <void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await initApp();
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    SystemChrome.setSystemUIOverlayStyle(const SystemUiOverlayStyle(
      //systemNavigationBarColor: Colors.transparent, // navigation bar color
      // statusBarColor: Colors.pink, // status bar color
      systemNavigationBarColor: Colors.transparent,
      systemNavigationBarIconBrightness: Brightness.light,
      statusBarColor: Colors.transparent,
      statusBarBrightness: Brightness.light,
      statusBarIconBrightness: Brightness.light,
    ));
    runApp(Phoenix(
        child: ScreenUtilInit(
            designSize: const Size(375.0, 667.0),
            builder: (_, child) {
              return const MaterialApp(
                debugShowCheckedModeBanner: false,
                home: MainCoffeeConceptApp(),
              );

            })));
  });
}
