import 'package:flutter/material.dart';
const   _initialPage = 8.0;

class CoffeeBloc{
  final pageCoffeeController = PageController(
    viewportFraction: 0.35,
    initialPage: _initialPage.toInt(),
  );
  final currentPage = ValueNotifier<double>(_initialPage) ;
  final textPage = ValueNotifier<double>(_initialPage) ;
  final pageTextController = PageController(initialPage: _initialPage.toInt(),);

  void _coffeeScrollListener(){
      currentPage.value = pageCoffeeController.page!;
  }


  void _textScrollListener(){
    textPage.value = pageTextController.page!;
  }

  void init() {
    // TODO: implement initState
    pageCoffeeController.addListener(_coffeeScrollListener);
    pageTextController.addListener(_textScrollListener);
  }
  void dispose() {
    // TODO: implement dispose
    pageCoffeeController.removeListener(_coffeeScrollListener);
    pageTextController.removeListener(_textScrollListener);
    pageCoffeeController.dispose();
    pageTextController.dispose();
  }


}

class CoffeeProvider extends InheritedWidget{
  final CoffeeBloc bloc;

  const CoffeeProvider({super.key, required this.bloc, required Widget child}): super(child: child);
  static CoffeeProvider? of(BuildContext context)=> context.findAncestorWidgetOfExactType<CoffeeProvider>();
  @override
  bool updateShouldNotify(covariant CoffeeProvider oldWidget)=>false;
}