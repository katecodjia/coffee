import 'package:flutter/material.dart';

import '../../../../core/domain/local/coffree.dart';
import '../../../utils/constants/space.dart';
class CoffeeConceptDetails extends StatefulWidget {
  const CoffeeConceptDetails({Key? key, required this.coffee}) : super(key: key);
  final Coffee coffee;
  @override
  State<CoffeeConceptDetails> createState() => _CoffeeConceptDetailsState();
}

class _CoffeeConceptDetailsState extends State<CoffeeConceptDetails> {
  @override
  Widget build(BuildContext context) {
  final Size  size = MediaQuery.of(context).size;
    return  Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: const BackButton(
          color: Colors.black,
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          Padding(
            padding: EdgeInsets.symmetric(horizontal:size.height *0.1 ),
            child: Hero(
              tag: "text_${widget.coffee.name}",
              child: Material(
                child: Text(
                  widget.coffee.name,
                  maxLines: 2,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
          ),
          const SpaceH(15),
          SizedBox(
            height: size.height * 0.4,
            child: Stack(
              children: [
                Positioned.fill(
                    child: Hero(
                      tag: widget.coffee.name,
                      child: Image.asset(
                        widget.coffee.image,
                        fit: BoxFit.fitHeight,
                      ),
                    )
                ),
                
                Positioned(
                  left: size.width * 0.05,
                    bottom: 0,
                    child: TweenAnimationBuilder<double>(
                        tween: Tween(begin: 1.0, end: 0.0),
                        duration: const Duration(milliseconds: 500),
                        builder: (context, value,child){
                          return Transform.translate(
                            offset: Offset(-100 * value, 240*value),
                            child: child,
                          );
                        },
                      child: Text(
                        '\$${widget.coffee.price.toStringAsFixed(2)}',
                        style: const TextStyle(
                          fontSize: 50,
                          fontWeight: FontWeight.w900,
                          color: Colors.white,
                          shadows: [
                            BoxShadow(
                              color: Colors.black45,
                              blurRadius: 10,
                              spreadRadius: 20,
                            )
                          ]
                        ),
                      ),
                    )
                ),

              ],
            ),
          )
        ],
      ),
    );
  }
}
