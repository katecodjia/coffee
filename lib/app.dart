import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:status_bar_control/status_bar_control.dart';

Future<void> initApp() async {
  WidgetsFlutterBinding.ensureInitialized();
  await SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge);
  await StatusBarControl.getHeight.then((value) {
    //AppService.instance.setAppHeight = value;
  });
 /* await StatusBarControl.setFullscreen(true);
  await StatusBarControl.setHidden(false);
  SystemChrome.setSystemUIChangeCallback((v) async {
    StatusBarControl.setHidden(false);
  });*/
}
