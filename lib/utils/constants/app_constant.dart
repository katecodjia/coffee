import 'dart:ui';

class AppConstant {
  AppConstant._internal();
  static const String appName = 'Ayooka';
  static const String folderName = appName;


  static const String onBoard = 'onboard';
  static const String login = 'login';
  static const String signup = 'signup';

  static const supportedLocales = [
    Locale('en'),
    Locale('fr'),
  ];


}
