import 'package:flutter/material.dart';

import '../color/app_color.dart';

class AppTypography {
  ///FontWeight w400
  //static const _dmSansBold = 'dmSansBold';
  ///FontWeight w700
  static const _dmSansBold = 'dmSansBold';
  ///FontWeight w500
  static const _dmSansMedium = 'dmSansMedium';
  ///FontWeight w400
  static const _dmSansRegular = 'dmSansRegular';


  static const _lobster = 'lobster';

  final heading1 = TextStyle(
      fontFamily: _dmSansBold,
      fontWeight: FontWeight.w700,
      fontSize: 22,
      color: $appColors.text);

  final heading2 = TextStyle(
      fontFamily: _dmSansBold,
      fontWeight: FontWeight.w700,
      fontSize: 16,
      color: $appColors.text);

  final heading5 = TextStyle(
      fontFamily: _dmSansBold,
      fontWeight: FontWeight.w700,
      fontSize: 18,
      color: $appColors.text);

  final heading3 = TextStyle(
      fontFamily: _dmSansBold,
      fontWeight: FontWeight.w700,
      fontSize: 14,
      color: $appColors.text);

  final heading4 = TextStyle(
      fontFamily: _dmSansBold,
      fontWeight: FontWeight.w700,
      fontSize: 12,
      color: $appColors.text);

  final body1 = TextStyle(
      fontFamily: _dmSansMedium,
      fontWeight: FontWeight.w500,
      fontSize: 16,
      color: $appColors.text);

  final body2 = TextStyle(
      fontFamily: _dmSansMedium,
      fontWeight: FontWeight.w500,
      fontSize: 14,
      color: $appColors.text);

  final body3 = TextStyle(
      fontFamily: _dmSansMedium,
      fontWeight: FontWeight.w500,
      fontSize: 12,
      color: $appColors.text);

  final body4 = TextStyle(
      fontFamily: _dmSansMedium,
      fontWeight: FontWeight.w500,
      fontSize: 10,
      color: $appColors.text);

  final text1 = TextStyle(
      fontFamily: _dmSansRegular,
      fontWeight: FontWeight.w400,
      fontSize: 16,
      color: $appColors.text);

  final text2 = TextStyle(
      fontFamily: _dmSansRegular,
      fontWeight: FontWeight.w400,
      fontSize: 14,
      color: $appColors.text);

  final text3 = TextStyle(
      fontFamily: _dmSansRegular,
      fontWeight: FontWeight.w400,
      fontSize: 12,
      color: $appColors.text);
  final text4 = TextStyle(
      fontFamily: _dmSansRegular,
      fontWeight: FontWeight.w400,
      fontSize: 10,
      color: $appColors.greyMedium);






/*  final heading3 = TextStyle(
      fontFamily: _notoSansBold,
      fontWeight: FontWeight.w700,
      fontSize: 16,
      color: $appColors.greyStrong);

  final heading2 = TextStyle(
      fontFamily: _notoSansBold,
      fontWeight: FontWeight.w700,
      fontSize: 14,
      color: $appColors.greyStrong);

  final heading9=TextStyle(
      fontFamily: _notoSansLigth,
      color: $appColors.black,
      fontWeight: FontWeight.w300,
      fontSize: 10
  );
  final heading10 = TextStyle(
      fontFamily: _notoSansMedium,
      fontWeight: FontWeight.w500,
      fontSize: 10,
      color: $appColors.accent1);

  final heading20 = TextStyle(
      fontFamily: _notoSansMedium,
      fontWeight: FontWeight.w500,
      fontSize: 12,
      color: $appColors.accent1,
      decoration: TextDecoration.underline,
  );

  final heading22 = TextStyle(
      fontFamily: _notoSansMedium,
      fontWeight: FontWeight.w500,
      fontSize: 12,
      color: $appColors.text,
  );

  final heading14 = TextStyle(
      fontFamily: _notoSansMedium,
      fontWeight: FontWeight.w500,
      fontSize: 14,
      color: $appColors.accent1);

  final heading16 = TextStyle(
      fontFamily: _notoSansMedium,
      fontWeight: FontWeight.w500,
      fontSize: 16,
      color: $appColors.white);

  final heading5 = TextStyle(
      fontFamily: _notoSansMedium,
      fontWeight: FontWeight.w500,
      fontSize: 24,
      color: $appColors.greyStrong);

  final text = TextStyle(
      fontFamily: _notoSans,
      fontWeight: FontWeight.w400,
      fontSize: 16,
      color: $appColors.accent1);

  final subtitle2 = TextStyle(
      fontFamily: _notoSans,
      fontWeight: FontWeight.w400,
      fontSize: 12,
      color: $appColors.text);

  final subtitle3 = TextStyle(
      fontFamily: _notoSans,
      fontWeight: FontWeight.w400,
      fontSize: 14,
      color: $appColors.hint);

  final textLigth = TextStyle(
      fontFamily: _notoSansLigth,
      fontWeight: FontWeight.w300,
      color: $appColors.greyStrong,
      fontSize: 16);

  final bodyLarge = TextStyle(
    fontFamily: _notoSansLigth,
    fontSize: 16,
    color: $appColors.black,
  );

  final lobster = TextStyle(
    fontFamily: _lobster,
    fontSize: 10,
    fontWeight: FontWeight.w400,
    color: $appColors.black,
  );




  final hintText = TextStyle(
      fontFamily: _notoSans,
      fontWeight: FontWeight.w300,
      fontSize: 16,
      color: $appColors.grey);

  final titleLarge = const TextStyle(
    fontFamily: _notoSansMedium,
    fontSize: 24,
    color: Colors.white,
  );
  final titleMedium = const TextStyle(
    fontFamily: _notoSansMedium,
    fontSize: 24,
    color: Colors.white,
  );
  final bodySmall = const TextStyle(
    fontFamily: _notoSansMedium,
    fontSize: 24,
    color: Colors.white,
  );

  final bodyMedium = const TextStyle(
    fontFamily: _notoSansMedium,
    fontSize: 24,
    color: Colors.white,
  );

  final labelLarge = const TextStyle(
    fontFamily: _notoSansMedium,
    fontSize: 24,
    color: Colors.white,
  );

  final heading1 = const TextStyle(
    fontFamily: _notoSansMedium,
    fontSize: 24,
    color: Colors.white,
  );

  final heading4 = const TextStyle(
    fontFamily: _notoSansBold,
    fontSize: 14,
    color: Colors.white,
  );

  final heading6 = const TextStyle(
    fontFamily: _notoSansMedium,
    fontSize: 10,
    color: Colors.black,
  );

  final heading7 = const TextStyle(
    fontFamily: _notoSansSemiBold,
    fontSize: 10,
    color: Colors.white,
  );


  final body1 = const TextStyle(
    fontFamily: _notoSansRegular,
    fontSize: 16,
    color: Colors.black,
  );

  final body2 = const TextStyle(
    fontFamily: _notoSansRegular,
    fontSize: 12,
    color: Colors.white,
  );

  final body3 = const TextStyle(
    fontFamily: _notoSansBold,
    fontSize: 14,
    color: Colors.white,
  );

  final body4 = const TextStyle(
    fontFamily: _notoSansRegular,
    fontSize: 12,
    color: Colors.white,
  );

  final body45 = const TextStyle(
    fontFamily: _notoSansRegular,
    fontSize: 10,
    color: Colors.white,
  );*/
}
