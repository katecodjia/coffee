import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import '../typography/app_typography.dart';

final $appColors = AppColors();

class AppColors {
  final Color accent1 = const Color(0xFFBE0316);
  final Color accent2 = const Color(0xFFF5E9E6);
  final Color offWhite = const Color(0xFFfffbf4);
  final Color suffix = const Color(0xFFE7E5E3);

  //final Color offWhite = const Color(0xFFf8f5f1);
  final Color icon = const Color(0xFF87898A);
  final Color button = const Color(0xFFD8D8D8);
  //final Color offWhite = const Color(0xFFF5E9E6);
  final Color grey = const Color(0xFF969595);
  final Color greyStrong = const Color(0xFF657783);
  final Color background = const Color(0xFFF5F5F5);
  final Color pureWhite = const Color(0xFF333333);
  final Color font = const Color(0xFFECF3EF);

  final Color greyColor = const Color(0xFFCDCCCC);
  final Color greyLite = const Color(0xFFBEC7D6);
  final Color pureBlack = const Color(0xFF000000);
  final Color divider = const Color(0xFFE5E5E5);
  final Color text = const Color(0xFF272D32);
  final Color title = const Color(0xFF010F18);
  final Color caption = const Color(0xFF7D7873);
  final Color body = const Color(0xFF514F4D);
  final Color border = const Color(0xFFCDCCCC);
  final Color greyMedium = const Color(0xFF9D9995);
  final Color white = Colors.white;
  final Color black = const Color(0xFF1E1B18);
  final Color greenDivider = const Color(0xFF222A36);
  final Color hint = const Color(0xFF969595);
  final Color green = const Color(0xFF27AE60);
  final bool isDark = false;

  Color shift(Color c, double d) => ColorUtils.shiftHsl(c, d * (isDark ? -1 : 1));

  ThemeData toThemeData() {
    /// Create a TextTheme and ColorScheme, that we can use to generate ThemeData
    TextTheme txtTheme = (isDark ? ThemeData.dark() : ThemeData.light()).textTheme;
    Color txtColor = white;
    ColorScheme colorScheme = ColorScheme(
        brightness: isDark ? Brightness.dark : Brightness.light,
        primary: accent1,
        primaryContainer: accent1,
        secondary: accent1,
        secondaryContainer: accent1,
        background: white,
        surface: offWhite,
        onBackground: txtColor,
        onSurface: txtColor,
        onError: Colors.white,
        onPrimary: Colors.white,
        onSecondary: Colors.white,
        error: Colors.red.shade400,
    );

    /// Now that we have ColorScheme and TextTheme, we can create the ThemeData
    /// Also add on some extra properties that ColorScheme seems to miss
    var t = ThemeData.from(
        useMaterial3: true,
        textTheme: txtTheme,
        colorScheme: colorScheme,

        //colorSchemeSeed:
    ).copyWith(
      textSelectionTheme: TextSelectionThemeData(cursorColor: accent1),
        highlightColor: accent1,
        splashColor: Colors.transparent,
        appBarTheme:  AppBarTheme(
          systemOverlayStyle: SystemUiOverlayStyle.light, // 2
          color: $appColors.white,
        ),
        textTheme: TextTheme(
        //  titleLarge: AppTypography().titleLarge,
          //titleMedium: AppTypography().titleMedium,
          //bodySmall: AppTypography().bodySmall,
          //bodyLarge: AppTypography().bodyLarge,
          bodyLarge: AppTypography().text2,
         // labelLarge: AppTypography().labelLarge,

        ),
      inputDecorationTheme: InputDecorationTheme(
        border: InputBorder.none,
        contentPadding: const EdgeInsets.fromLTRB(10, 5, 10, 10),
        enabledBorder: OutlineInputBorder(
          borderSide: BorderSide(
            style: BorderStyle.solid,
            color: $appColors.border
          ),
          borderRadius: BorderRadius.circular(4.0),
        ),
        focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(4.0),
            borderSide: BorderSide(color: $appColors.accent1)),
        hintStyle: AppTypography().text2.copyWith(color: $appColors.grey),
      )
    );

    /// Return the themeData which MaterialApp can now use
    return t;
  }
}

class ColorUtils {
  static Color shiftHsl(Color c, [double amt = 0]) {
    var hslc = HSLColor.fromColor(c);
    return hslc.withLightness((hslc.lightness + amt).clamp(0.0, 1.0)).toColor();
  }

  static Color parseHex(String value) => Color(int.parse(value.substring(1, 7), radix: 16) + 0xFF000000);

  static Color blend(Color dst, Color src, double opacity) {
    return Color.fromARGB(
      255,
      (dst.red.toDouble() * (1.0 - opacity) + src.red.toDouble() * opacity).toInt(),
      (dst.green.toDouble() * (1.0 - opacity) + src.green.toDouble() * opacity).toInt(),
      (dst.blue.toDouble() * (1.0 - opacity) + src.blue.toDouble() * opacity).toInt(),
    );
  }
}